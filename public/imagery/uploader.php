<?php
  header('Content-type:application/json');
  //print '{"files":[{"name":1}]}';
  $uploads_dir = __DIR__.'/uploads';
  foreach ($_FILES["files"]["error"] as $key => $error) {
      if ($error == UPLOAD_ERR_OK) {
          $tmp_name = $_FILES["files"]["tmp_name"][$key];
          $name = $_FILES["files"]["name"][$key];
          move_uploaded_file($tmp_name, "$uploads_dir/$name");
          $content = file("$uploads_dir/$name");
          $elements = array();
          foreach($content as $url) {
              $ue = trim($url);
              $fp = @fopen($ue,"r");
              $exists = !!$fp;
              if($exists) {
                  $a = 1.35;//FIXME
                  $info_u = getImageSize(trim($ue));
                  $a = (float)$info_u[0] / (float)$info_u[1];
$elements []= <<<URL
                  {"name":"$ue", "exists":$exists, "aspect":$a}
URL;
                  fclose($fp);
              }
          }
          $elements = join(',', $elements);
          echo "{\"files\":[$elements]}";
      }

  }
