var initialized = false;
function borderWidth() { return 6.0 }
function displayUnits() { return 'px' }
function fixedHeight() { return 200.0 }
function appendImage(file) {
    if($('div[data-source="'+(file.name)+'"]').data('source')) {
        return;
    }
    $('<div/>').css("margin","0")
        .attr('class', 'tile-element')
        .css("background-size", "cover")
        .css("height",fixedHeight() + displayUnits())
        .css("width", (fixedHeight() * file.aspect)+displayUnits())
        .css("float","left")
        .css("display", "inline")
        .css("background-position", "center")
        .css("background-image",'url(imagery/watermark.php?url='+file.name+')')
        .attr("data-natural-width", Math.floor(file.aspect * fixedHeight()))
        .attr("data-source", file.name)
        .appendTo($('#images-container'));
}
$(function () {
    
    $(window).resize(function(){ adjustPictureSizes() } );
    $('#fileupload').fileupload({
        dataType: 'json',
        add: function (e, data) {
            initialized = false;
            data.submit();
            $('.has-spinner').addClass('active');
        },
        done: function (e, data) {
            $('#images-container').empty();
            $.each(data.result.files, function (index, file) {
                if(file.exists) {
                    appendImage(file);
                }
            });
            initialized = true;
            adjustPictureSizes();
            $('.has-spinner').removeClass('active');
        },
        error: function(e, data) {
            $('.has-spinner').removeClass('active');
            alert(JSON.stringify(e) + ': ' + JSON.stringify(data));
        },
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator && navigator.userAgent),
        imageMaxWidth: 200,
        imageMaxHeight: 200,
        imageCrop: true // Force cropped images 
    });
});
function adjustPictureSizes() {
    if(!initialized) return;
    var rows = [[]];
    var containerWidth = $('#images-container-container').width();
    var c = $('#images-container').children();
    var rowSum = 0.0, rowNumber = 0, rowArray = [];
    var i = 0;
    for (var i = 0; i < c.length; i++) {
        var img = $(c[i]);
        if(rowSum == 0) img.addClass("clearLeft"); else img.removeClass("clearLeft");
        rowSum += img.data().naturalWidth;
        img.data().adjustWidth = img.data().naturalWidth;
        rowArray.push (img);
        if( rowSum > containerWidth) {
            rows[rowNumber] = rowArray;
            rowNumber ++; 
            rowSum = 0.0;
            rowArray = [];
        }
    }

    for (var r = 0; r < rows.length; r++) {
         var counter = 0, curSum;

         do {
            var col = counter++ % rows[r].length;
            rows[r][col].data().adjustWidth --;
            curSum = 0;
            rows[r].forEach(function(el) { curSum+=el.data().adjustWidth });
            rows[r].forEach(function(el) { el.data().rowWidthSum = curSum });
         }
         while (curSum >= containerWidth && counter < 0x1000);
         rows[r].forEach(function(el) { 
            el.css('width', Math.floor(el.data().adjustWidth) + 'px');
         });
    }
}
