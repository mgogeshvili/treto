<?php
if (array_key_exists('aspectInfo', $_GET)) {
    aspectInfo($_GET['url']);
}
else {
    waterMark($_GET['url'], __DIR__."/watermarks/treto.png", 0, 3);
}
exit; //END MAIN
function aspectInfo($original) {
    $original = urldecode($original);
    $info_o = getImageSize($original);
    if (!$info_o) {
        http_response_code(404); 
        return false;
    }
    print '{aspect:' . $info_o[0] . '/' . $info_o[1].'}';
}
function savePrefix()
{
   return __DIR__.'/'.'uploads'.'/';
}
function waterMark($original, $watermark, $sx, $sy)
{
    $saveFile = savePrefix().basename($original);
    $original = urldecode($original);
    $info_o = getImageSize($original);
    $info_w = getImageSize($watermark);
    if (!$info_o || !$info_w){
        http_response_code(404); 
        return false;
    }

    $y = ceil(($info_o[1] - $info_w[1])/2) + (int)$sy;
    $x = ceil(($info_o[0] - $info_w[0])/2) + (int)$sx;

    header("Content-Type: ".$info_o['mime']);
    
    $original = imageCreateFromString(file_get_contents($original));
    //if(file_exists($saveFile)) { passthru ($saveFile); exit; }
    $watermark = imageCreateFromString(file_get_contents($watermark));
    $out = imageCreateTrueColor($info_o[0],$info_o[1]);

    imageCopy($out, $original, 0, 0, 0, 0, $info_o[0], $info_o[1]);

    if( ($info_o[0] > 100) && ($info_o[1] > 100) ) {
        $percent = floor( 100.0 * ($info_o[0] - 30) / $info_w[0]  ) / 100.0;
        $newWidth = ceil($info_w[0] * $percent);
        $newHeight = ceil($info_w[0] * $percent);

        imageCopyResized($out, $watermark, $x, $y, 0, 0, 
          $newWidth, $newHeight,
          $info_w[0], 
          $info_w[1]
        );
    }
    switch ($info_o[2]) {
        case 1:
            imageGIF($out);
            //imageGIF($out, $saveFile);
            break;
        case 2:
            imageJPEG($out);
            //imageJPEG($out, $saveFile);
            break;
        case 3:
            imagePNG($out);
            //imagePNG($out, $saveFile);
            break;
    }

    imageDestroy($out);
    imageDestroy($original);
    imageDestroy($watermark);

    return true;
}

