<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Imagelist Upload</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="static/css/jquery.fileupload.css">
    <link rel="stylesheet" href="static/css/main.css">
</head>
<body>
<div id="images-container-container">
    <!-- The fileinput-button span is used to style the file input field as button -->
    <span class="btn btn-success fileinput-button has-spinner">
        <i class="icon-refresh icon-spin spinner"></i>
        <span>Upload a picture URL list</span>
        <!-- The file input field used as target for the file upload widget -->
        <input id="fileupload" type="file" name="files[]" data-url="imagery/uploader.php" multiple>
    </span>
    <div id="images-container"></div>
</div>
<style>
.clearLeft {
    clear: left!important;
    border-left-width:0;
}
</style>

<script src="js/jquery/1.10.1/jquery.min.js"></script>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="js/vendor/jquery.ui.widget.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="js/jquery.fileupload-image.js"></script>
<script src="js/script.js"></script>
</body> 
</html>
